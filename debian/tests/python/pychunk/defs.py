"""Definitions for the Python zchunk tests."""

from __future__ import annotations

import typing


if typing.TYPE_CHECKING:
    from typing import Final


MAGIC: Final = bytes([0, ord("Z"), ord("C"), ord("K"), ord("1")])
